import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SushiComponent } from './components/sushi/sushi.component';



const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'sushi', component: SushiComponent },
    
    

    { path: '**', pathMatch: 'full', redirectTo:'home' }  
];


/* export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { relativeLinkResolution: 'legacy' }); */