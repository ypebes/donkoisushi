import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SushiComponent } from './components/sushi/sushi.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'sushi', component: SushiComponent },

  

  { path: '**', pathMatch: 'full', redirectTo:'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
